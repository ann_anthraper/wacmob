//
//  WACRestManager.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import Alamofire

class WACRestManager {
    
    static func getMockupData(successfulResponse: @escaping (DataResponse<Data>) -> Void) {
        let url = WACConstants.mockup_url
        Alamofire.request(url,
                          method:.get,
                          encoding: JSONEncoding.default)
            .responseData { (response) in
                successfulResponse(response)
        }
    }
}
