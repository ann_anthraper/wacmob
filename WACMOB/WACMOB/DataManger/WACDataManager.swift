//
//  WACDataManager.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import SwiftyJSON

class WACDataManager {
        
    static func getMockupData(completionBlock: @escaping((Bool) -> ()), failure:@escaping((NSError) -> ())) {

        WACInteractor.getMockupData(successfulResponse: { (response) in
            let json = JSON(response as Any)
            if json.count > 0 {
                if json["result"]["offers"].count > 0 {
                    WACConstants.wac_offers.removeAll()
                    for i in 0 ..< json["result"]["offers"].count {
                        WACConstants.wac_offers.append(Category(category: json["result"]["offers"][i]["offer_category"].stringValue , image_url:json["result"]["offers"][i]["offers_url"].stringValue))
                    }
                }
                if json["result"]["promotions"].count > 0 {
                    WACConstants.wac_promotions.removeAll()
                    for i in 0 ..< json["result"]["promotions"].count {
                        WACConstants.wac_promotions.append(Promotions(name: json["result"]["promotions"][i]["promotions_name"].stringValue, image_url: json["result"]["promotions"][i]["photos_url"].stringValue, about_promo: json["result"]["promotions"][i]["about_promotions"].stringValue))
                    }
                }
                completionBlock(true)
            } else {
                completionBlock(false)
            }
        }) { (error) in
            failure(error)
        }
        
    }
}
 
