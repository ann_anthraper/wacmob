//
//  Helper.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    static func downloadImageFromURL(url:String, imgView:UIImageView){
        let link = URL(string: url)
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: link!)
            if data != nil{
                DispatchQueue.main.async {
                    imgView.image = UIImage(data: data!)
                }
            }
        }
    }
}
