//
//  PromotionsCollectionViewCell.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import UIKit

class PromotionsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var about: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        self.imageView.layer.cornerRadius = 10
        self.imageView.clipsToBounds = true
        self.imageView.layer.masksToBounds = true
    }
}
