//
//  WACConstants.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation

struct WACConstants {
    
    static let mockup_url = "http://www.mocky.io/v2/5c1a39e53200006a0064b01b"
    
    static var wac_offers = [Category]()
    static var wac_promotions = [Promotions]()
    
    static var detailData: Detail = Detail.example
    
}
