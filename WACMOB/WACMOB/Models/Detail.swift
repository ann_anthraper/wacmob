//
//  Detail.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation

struct Detail {
    
    var name: String
    var image_url: String
    var about: String
    
    #if DEBUG
    static let example = Detail(name: "Olive Garden", image_url: "http://binoop.webc.in/ios-machine-test/images/Image9.png", about: "The Olive Garden’s use of a slideshow that fits the width of your screen is an excellent method of bringing the cuisine front and center. Current specials are listed – another tactic useful in driving more hungry customers through the door. There’s no need to draw up Google Maps to find the nearest location, as an overlayed map provides that necessary info for you right on the homepage.")
    #endif
    
}
