//
//  HomeViewController.swift
//  WACMOB
//
//  Created by Ann Mary on 23/07/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var promotionsCollectionView: UICollectionView!
    
    
    //MARK:- View Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
        
        self.promotionsCollectionView.delegate = self
        self.promotionsCollectionView.dataSource = self
        
        getMockupData()
    }
    
    
    //MARK:- Custom Methods
    
    func getMockupData() {
        
        WACDataManager.getMockupData(completionBlock: { (status) in
            if status {
                DispatchQueue.main.async {
                    self.categoryCollectionView.reloadData()
                    self.promotionsCollectionView.reloadData()
                }
            }
        }) { (error) in
            print("Mockup API failed with error : \(error)")
        }
    }
    
}

//MARK:- Collection View Methods

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            WACConstants.detailData = Detail(name: WACConstants.wac_offers[indexPath.row].category, image_url: WACConstants.wac_offers[indexPath.row].image_url  , about: "")
        } else {
            WACConstants.detailData = Detail(name: WACConstants.wac_promotions[indexPath.row].name, image_url: WACConstants.wac_promotions[indexPath.row].image_url , about: WACConstants.wac_promotions[indexPath.row].about_promo)
        }
        self.performSegue(withIdentifier: "detail", sender: collectionView)

    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView {
            return WACConstants.wac_offers.count
        } else {
            return WACConstants.wac_promotions.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == categoryCollectionView {
            let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "category", for: indexPath) as! CategoryCollectionViewCell
            
            cell.layoutIfNeeded()
            
            Helper.downloadImageFromURL(url: WACConstants.wac_offers[indexPath.row].image_url, imgView: cell.imageView)
            
            cell.imageView.layer.cornerRadius = 10.0
            cell.imageView.layer.masksToBounds = true
            cell.title.text = WACConstants.wac_offers[indexPath.row].category
            
            return cell
        }
        else{
            let cell = promotionsCollectionView.dequeueReusableCell(withReuseIdentifier: "promotions", for: indexPath) as! PromotionsCollectionViewCell

            Helper.downloadImageFromURL(url: WACConstants.wac_promotions[indexPath.row].image_url, imgView: cell.imageView)
            
            cell.title.text = WACConstants.wac_promotions[indexPath.row].name
            cell.about.text = WACConstants.wac_promotions[indexPath.row].about_promo

            return cell
        }
    }
    
}
